##### VMs Part #####

resource "yandex_compute_instance" "srv1" {
  name        = "srv1"
  platform_id = "standard-v2"
  resources {
    core_fraction = 100
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "srv2" {
  name        = "srv2"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "srv3" {
  name        = "srv3"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "test" {
  name        = "test"
  platform_id = "standard-v2"
  resources {
    core_fraction = 5
    cores         = "2"
    memory        = "1"
  }
  boot_disk {
    initialize_params {
      image_id = "fd8evlqsgg4e81rbdkn7" # ubuntu-22
      size     = 8
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}


##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[srv1]
${yandex_compute_instance.srv1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[srv2]
${yandex_compute_instance.srv2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[srv3]
${yandex_compute_instance.srv3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[test]
${yandex_compute_instance.test.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[all]
${yandex_compute_instance.srv1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.srv2.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.srv3.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.test.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
EOF
  filename = "${path.module}/inventory"
}

##### Create files elasticsearch.yml #####

resource "local_file" "elastic1" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-1
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.srv1.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv2.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/tmp/elastic-srv1.yml"
}

resource "local_file" "elastic2" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-2
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.srv1.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv2.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/tmp/elastic-srv2.yml"
}

resource "local_file" "elastic3" {
  content  = <<EOF
network.host: [_eth0_, _local_]
node.name: elasticsearch-3
cluster.name: elasticsearch_cluster
cluster.initial_master_nodes:
        - elasticsearch-1
        - elasticsearch-2
        - elasticsearch-3
discovery.seed_hosts:
        - ${yandex_compute_instance.srv1.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv2.network_interface.0.ip_address}
        - ${yandex_compute_instance.srv3.network_interface.0.ip_address}
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
EOF
  filename = "${path.module}/tmp/elastic-srv3.yml"
}

##### Provisioning with Ansible VM srv1 #####

resource "null_resource" "srv1" {
  depends_on = [yandex_compute_instance.srv1, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv1.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-srv1.yml"
  }
}

resource "null_resource" "srv2" {
  depends_on = [yandex_compute_instance.srv2, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv2.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-srv2.yml"
  }
}

resource "null_resource" "srv3" {
  depends_on = [yandex_compute_instance.srv3, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv3.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-srv3.yml"
  }
}

resource "null_resource" "test" {
  depends_on = [yandex_compute_instance.test, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.test.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-test.yml"
  }
}

##### Add A-records to DNS zone #####

resource "yandex_dns_recordset" "srv1_dns_name" {
  depends_on = [yandex_compute_instance.srv1, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "srv1"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.srv1.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "srv2_dns_name" {
  depends_on = [yandex_compute_instance.srv2, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "srv2"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.srv2.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "srv3_dns_name" {
  depends_on = [yandex_compute_instance.srv3, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "srv3"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.srv3.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "test_dns_name" {
  depends_on = [yandex_compute_instance.test, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "test"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.test.network_interface.0.nat_ip_address]
}
